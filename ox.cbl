       IDENTIFICATION DIVISION. 
       PROGRAM-ID. OX.
       AUTHOR. THANAWAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  WS-ROW OCCURS 3 TIMES .
           05 WS-COL   PIC X OCCURS 3 TIMES VALUE "-".

       01  WS-IDX-ROW     PIC 9.
       01  WS-IDX-COL     PIC 9.
       01  WS-INPUT-ROW   PIC 9.
           88 WS-INPUT-ROW-VALID   VALUE 1 THRU 3.
       01  WS-INPUT-COL   PIC 9.
           88 WS-INPUT-COL-VALID   VALUE 1 THRU 3.
       01  WS-COUNT       PIC 9(2) VALUE ZEROS.
       01  WS-PLAYER      PIC X VALUE "X".

       01  WS-PLAYER-CHECK      PIC X VALUE "X".
       01  WS-ROW-COUNT-CHECK   PIC 9(2) VALUE 1.
       01  WS-COL-COUNT-CHECK   PIC 9(2) VALUE 0.
       01  WS-COUNTER-CHECK     PIC 9 VALUE ZEROS.

       PROCEDURE DIVISION.
       BEGIN.
           PERFORM UNTIL WS-COUNT>9
      *       DISPLAY "Count: " WS-COUNT
              IF WS-COUNT = 9
                 PERFORM DIS-DRAW 
              END-IF
              PERFORM DISPLAY-TURN
              PERFORM INPUT-ROW-COL
              PERFORM PUT-TABLE
              PERFORM DISPLAY-TABLE
              PERFORM RESET-ROW-COL
              PERFORM CHECK-WIN
              PERFORM TURN-PLAYER-CHECK 
              PERFORM CHECK-WIN
           END-PERFORM

           GOBACK
       .

       DIS-DRAW.
           DISPLAY "======"
           DISPLAY "-Draw-"
           STOP RUN
       .

       CHECK-WIN.
           MOVE ZEROS TO WS-COL-COUNT-CHECK
           PERFORM CHECK-COL 3 TIMES 
           MOVE ZEROS TO WS-ROW-COUNT-CHECK
           PERFORM CHECK-ROW 3 TIMES 
           PERFORM CHECK-X-LEFT 
           PERFORM CHECK-X-RIGHT 
       .

       CHECK-COL.
      *    Bing Col Check
           COMPUTE WS-COL-COUNT-CHECK = WS-COL-COUNT-CHECK + 1
           MOVE 1 TO WS-ROW-COUNT-CHECK
           PERFORM VARYING WS-ROW-COUNT-CHECK FROM 1 BY 1
              UNTIL WS-ROW-COUNT-CHECK = 4
                 IF WS-COL(WS-ROW-COUNT-CHECK,WS-COL-COUNT-CHECK) 
                 = WS-PLAYER-CHECK 
                    COMPUTE WS-COUNTER-CHECK = WS-COUNTER-CHECK + 1
                 END-IF
      *          DISPLAY "Count: " WS-ROW-COUNT-CHECK 
           END-PERFORM
           PERFORM COUNT-CHECK 
       .

       CHECK-ROW.
      *    Bing Row Check
           COMPUTE WS-ROW-COUNT-CHECK = WS-ROW-COUNT-CHECK + 1
           MOVE 1 TO WS-COL-COUNT-CHECK
           PERFORM VARYING WS-COL-COUNT-CHECK FROM 1 BY 1
              UNTIL WS-COL-COUNT-CHECK = 4
                 IF WS-COL(WS-ROW-COUNT-CHECK,WS-COL-COUNT-CHECK) 
                 = WS-PLAYER-CHECK 
                    COMPUTE WS-COUNTER-CHECK = WS-COUNTER-CHECK + 1
                 END-IF
      *          DISPLAY "Count: " WS-ROW-COUNT-CHECK 
           END-PERFORM
           PERFORM COUNT-CHECK 
       .

       CHECK-X-LEFT.
      *    Bing X Left
           IF WS-COL(1,1) = WS-PLAYER-CHECK 
              IF WS-COL(2,2) = WS-PLAYER-CHECK 
                 IF WS-COL(3,3) = WS-PLAYER-CHECK 
                    MOVE 3 TO WS-COUNTER-CHECK
                 END-IF
              END-IF
           END-IF
           PERFORM COUNT-CHECK 
       .

       CHECK-X-RIGHT.
      *    Bing X Right
           IF WS-COL(1,3) = WS-PLAYER-CHECK 
              IF WS-COL(2,2) = WS-PLAYER-CHECK 
                 IF WS-COL(3,1) = WS-PLAYER-CHECK 
                    MOVE 3 TO WS-COUNTER-CHECK
                 END-IF
              END-IF
           END-IF
           PERFORM COUNT-CHECK 
       .

       COUNT-CHECK.
           IF WS-COUNTER-CHECK = 3
              DISPLAY "======="
              DISPLAY "-" WS-PLAYER-CHECK " WIN-"
              STOP RUN
           END-IF
           MOVE ZEROS TO WS-COUNTER-CHECK 
       .

       TURN-PLAYER-CHECK.
           IF WS-PLAYER-CHECK = "X"
              MOVE "O" TO WS-PLAYER-CHECK 
           ELSE
              MOVE "X" TO WS-PLAYER-CHECK 
           END-IF
       .

       PUT-TABLE.
           IF WS-COL(WS-INPUT-ROW, WS-INPUT-COL) = "-"
              MOVE WS-PLAYER TO WS-COL(WS-INPUT-ROW, WS-INPUT-COL)
              ADD 1 TO WS-COUNT
              PERFORM TURN-PLAYER 
           ELSE
              DISPLAY "ERROR"
           END-IF 

       .

       DISPLAY-TURN.
           DISPLAY "TURN " WS-PLAYER 
       .

       TURN-PLAYER.
           IF WS-PLAYER = "X"
              MOVE "O" TO WS-PLAYER
           ELSE
              MOVE "X" TO WS-PLAYER
           END-IF
       .

       RESET-ROW-COL.
           MOVE ZEROS TO WS-INPUT-ROW, WS-INPUT-COL 
       .

       INPUT-ROW-COL.
           PERFORM UNTIL WS-INPUT-ROW-VALID    
              DISPLAY "INPUT ROW -"
              ACCEPT WS-INPUT-ROW 
           END-PERFORM
           PERFORM UNTIL WS-INPUT-COL-VALID
              DISPLAY "INPUT COL -"
              ACCEPT WS-INPUT-COL 
           END-PERFORM
       .

       DISPLAY-TABLE.
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              PERFORM VARYING WS-IDX-COL FROM 1 BY 1
                 UNTIL WS-IDX-COL > 3
                 DISPLAY WS-COL(WS-IDX-ROW, WS-IDX-COL)
                    WITH NO ADVANCING 
              END-PERFORM
              DISPLAY " "
           END-PERFORM
       .